defmodule Updater do
    use GenServer

    def start_link do
        GenServer.start_link __MODULE__, [], name: __MODULE__
    end
    
    def init state do
        {:ok, state}
    end
        
    def handle_call({:update, name, text}, _from, state) do
        :sys.suspend Calculator
        {:ok, file} = File.open "lib/#{name}", [:write]
        IO.binwrite file, text
        File.close file
        Code.compile_file "lib/#{name}"
        :sys.change_code Calculator, Calculator, "1", nil
        :sys.resume Calculator
        {:reply, :ok, state}
    end
    
    def test() do
        GenServer.cast(Updater, {:update, "module", "dawdaw\ndawdawdadwa"})
    end

end