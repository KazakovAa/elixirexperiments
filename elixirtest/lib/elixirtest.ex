defmodule Calculator do
  use GenServer

  def start_link do
    GenServer.start_link __MODULE__, [], name: __MODULE__
  end

  def init state do
    {:ok, state}
  end

  def add(x, y) do
    x + y
  end

  def sub(x, y) do
    x - y
  end
end