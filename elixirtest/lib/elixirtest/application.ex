defmodule Elixirtest.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  import Supervisor.Spec, warn: true
  use Application

  def start(_type, _args) do
    children = [

      worker(Calculator, []),
      worker(Updater, [])
      # Starts a worker by calling: Elixirtest.Worker.start_link(arg)
      # {Elixirtest.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Elixirtest.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
