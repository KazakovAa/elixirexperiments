defmodule Incrementorworker.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  import Supervisor.Spec, warn: true
  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    # children = [
    #   worker(Incrementorworker, ["1"], id: 1),

    # ]
    children = for child_id <- 1..20, do: worker(Incrementorworker, [Integer.to_string(child_id)], id: child_id)
    opts = [strategy: :one_for_one, name: Incrementorworker.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
