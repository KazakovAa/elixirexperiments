defmodule Incrementorworker do
  use GenServer

  def start_link(id) do
    GenServer.start_link(__MODULE__, %{:count => 0, :id => id}, name: :"#{__MODULE__}.#{id}")
  end

  def init(state) do
    # IO.inspect :"#{__MODULE__}.#{state.id}"
    {:ok, state}
  end

  def handle_cast({:inc}, state) do
    newState = %{state | count: (state.count + 1)}
    if newState.count == 10 do
      GenServer.cast {Incrementorboss, :""}, {:response, :done}
    end
    if newState.count < 10 do
      GenServer.cast {Incrementorboss, :""}, {:response, :no}
    end
    
    {:noreply, newState}
  end
end
