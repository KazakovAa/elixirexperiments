defmodule Insertfunctions do

    use GenServer

    def start_link do
        GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
    end

    def init(state) do
        {:ok, state}
    end

    def appendFunction(name, code, state) do
        {result, _} = Code.eval_string(code)
        Map.put state, name, result
    end

    def handle_call({:run, name_func, args}, _from, state) do
        result = state[name_func].(args)
        {:reply, result, state}
    end

    def handle_call({:add_function, name, code}, _from, state) do
        newstate = appendFunction(name, code, state)
        {:reply, :ok, newstate}
    end

    def handle_call({:state}, _from, state) do
        {:reply, state, state}
    end
end
