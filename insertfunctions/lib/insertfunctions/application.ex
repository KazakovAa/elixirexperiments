defmodule Insertfunctions.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  import Supervisor.Spec, warn: true
  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      worker(Insertfunctions, [])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Insertfunctions.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
