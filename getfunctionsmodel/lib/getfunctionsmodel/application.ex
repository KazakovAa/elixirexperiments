defmodule Getfunctionsmodel.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  import Supervisor.Spec, warn: true
  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: Getfunctionsmodel.Worker.start_link(arg)
      worker(Getfunctionsmodel, [])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Getfunctionsmodel.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
