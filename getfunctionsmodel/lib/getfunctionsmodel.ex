defmodule Getfunctionsmodel do
    use GenServer

    def start_link do
        GenServer.start_link(__MODULE__, [], name: __MODULE__)
    end 

    def init(state) do
        {:ok, state}
    end
# 'Droid Sans Mono', 'monospace', monospace, 'Droid Sans Fallback'

    def getInformationAboutFunctions() do
        node =
            Node.list() |>
            Enum.at 0

        GenServer.call({Getfunctionsmodel, node}, {:give_your_funcs}) # return
    end

    def handle_call({:give_your_funcs}, _from, state) do
        {:reply, __MODULE__.__info__(:functions), state}
    end

end
