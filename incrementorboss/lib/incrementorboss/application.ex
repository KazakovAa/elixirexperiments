defmodule Incrementorboss.Application do
  import Supervisor.Spec, warn: true
  use Application

  def start(_type, _args) do
    children = [
      worker(Incrementorboss, [])
    ]

    opts = [strategy: :one_for_one, name: Incrementorboss.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
