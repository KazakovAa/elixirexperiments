defmodule Incrementorboss do
  use GenServer
  def start_link do
    GenServer.start_link __MODULE__, %{sum: 0}, name: __MODULE__

  end

  def init(state) do
    {:ok, state}
  end

  def startInc do
    listModules = for id <- 1..20, do: :"Elixir.Incrementorworker.#{id}" 
    Enum.each Node.list, fn x -> 
      Enum.each listModules, fn y -> 
        spawn __MODULE__, :incLoop, [x,y]
      end
    end
  end

  def incLoop(x, y) do
    Enum.each 1..100, fn i -> 
      GenServer.cast {y, x}, {:inc}
    end
  end

  def handle_cast({:response, status}, state) do
    case status do
      :done -> 
        IO.puts "d1"
        if (state.sum + 10) >= 10000 do
          IO.puts "DONE!!!!"
        end
        {:noreply, %{state | sum: state.sum + 10 } }
      :no -> 
        IO.inspect "#{state.sum}"
        {:noreply, state}
    end
    
  end
end
