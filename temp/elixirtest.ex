defmodule Calculator do
  use GenServer

  def start_link do
    GenServer.start_link __MODULE__, %{key1: 1, key2: 4}, name: __MODULE__
  end

  def init state do
    {:ok, state}
  end

  def add(x, y) do
    x + y
  end


  def handle_call({:print_state}, _from, state) do
    IO.inspect state
    {:reply, :ok, state}
  end

  def handle_call({:change_state}, _from, state) do
    {:reply, :ok, %{state | key1: state[:key1] + 10}}
  end
end