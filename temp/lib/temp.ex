defmodule Temp do
  use GenServer

  def start_link do
    GenServer.start_link __MODULE__, [], name: __MODULE__
  end

  def init state do
    {:ok, state}
  end

  def updateRemotely(fileName) do
    node = Enum.at Node.list, 0

    {:ok, code} = File.read fileName
    GenServer.call {Updater, node}, {:update, fileName, code}
  end

end
